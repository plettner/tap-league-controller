import json

class JsonUtils:

    INDENT_AMOUNT=2

    @staticmethod
    def json(obj):
        if (obj != None):
            return json.dumps(obj.__dict__, indent=JsonUtils.INDENT_AMOUNT, default=str)
        else:
            return ""


    @staticmethod
    def jsonList(list):
        if (list != None):
            return json.dumps([obj.__dict__ for obj in list], indent=JsonUtils.INDENT_AMOUNT, default=str)
        else:
            return ""