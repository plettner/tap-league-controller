from mysql.connector import pooling
from mysql.connector import Error
import configparser

"""
Encapsulation of a database connection pool to protect our 
code from concrete links to a specific database implementation.
"""
class DatabaseConnectionPool:

    pool = None

    """
    Constructor takes a pool name and pool size
    """
    def __init__(self, name, size):
        try:
            configuration = self.getConfiguration()
            if name != None:
                configuration["pool_name"] = name
            if size != None:
                configuration["pool_size"] = size
            self.pool = pooling.MySQLConnectionPool(**configuration)
        except Error as exception:
            print("Error while attempting to connect to the database: ", exception)

    """
    This method loads the database pool configuration from
    the file application.ini.  Should probably be parameterized
    at some point, or not.
    """
    def getConfiguration(self):
        config = configparser.ConfigParser()
        config.read('application.ini')

        configuration = {}
        configuration["database"]  = config["DBCP"]["database"]
        configuration["user"]      = config["DBCP"]["user"]
        configuration["password"]  = config["DBCP"]["password"]
        configuration["pool_name"] = config["DBCP"]["pool_name"]
        configuration["pool_size"] = int(config["DBCP"]["pool_size"])
        return configuration


    """
    Returns a connection to the database
    """
    def getConnection(self):
        return self.pool.get_connection()

    """
    Convenience method for closing both a cursor and connection
    """
    def close(self, cursor, connection):
        try:
            if cursor != None:
                cursor.close()
        except:
            print("Exception while closing cursor")
            raise

        try :
            if connection != None and connection.is_connected():
                connection.close()

        except :
            print("Exception while closing connection")
            raise

"""
Simple main method that performs a simple test 
on our connection pooling class.
"""
if __name__ == '__main__':
    connectionPool = DatabaseConnectionPool("myPool", 10)
    connection = connectionPool.getConnection()
    print("Connect status : " + str(connection.is_connected()))
    
    connection2 = connectionPool.getConnection()
    print("Connect2 status : " + str(connection2.is_connected()))
    