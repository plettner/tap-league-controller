# League Controller

This project provides a RESTful endpoint for League data in 
the "Taprageous" project.  It serves up League data and only
League data.

# Requirements

This project uses python3, and a Python module called FastAPI which 
manages our REST controller code endpoints for us.  It's easy to use 
and quick to install.

Here's where you can find the [installation instructions](https://fastapi.tiangolo.com/#installation).

But they're (at this writing) quite simple:

    pip install fastapi

    pip install "uvicorn[standard]"

You'll also need to install the MySQL connector.

    pip install mysql-connector-python

# Setup

In your MySQL installation, you'll want to run the script in `sql/schema.ddl` so that you'll have the necessary tables and data to run this service.

Then make a copy of `application.ini_TEMPLATE` called 
to `application.ini`.  Add your database credentials to `application.ini`.  Do not commit this file to your repository.


# Run the Project

This project needs the uvicorn container app to run.  Start this project
with the following command:

    uvicorn LeagueController:app --reload

