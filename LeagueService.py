import json
from DBCP import DatabaseConnectionPool
from League import League
from JsonUtils import JsonUtils
"""
Encapsulation of methods for access League information
"""
class LeagueService:

    pool = None

    def __init__(self, databaseConnectionPool):
        self.pool = databaseConnectionPool

    def getLeague(self, id):
        query = "select leagueId, userId, name, nameNormalized, dateAdded from league where leagueId = %(leagueId)s"

        try:
            connection = self.pool.getConnection()
            cursor = connection.cursor()
            data = {'leagueId' : id}
            cursor.execute(query, data)
            for (leagueId, userId, name, nameNormalized, dateAdded) in cursor:
                return League(leagueId, userId, name, nameNormalized, dateAdded)

        finally:
            self.pool.close(cursor, connection)


    def list(self):
        query = "select leagueId, userId, name, nameNormalized, dateAdded from league order by leagueId"

        leagues = []

        try:
            connection = self.pool.getConnection()
            cursor = connection.cursor()
            cursor.execute(query)

            for (leagueId, userId, name, nameNormalized, dateAdded) in cursor:
                leagues.append(League(leagueId, userId, name, nameNormalized, dateAdded))
            return leagues

        finally:
            self.pool.close(cursor, connection)



if __name__ == '__main__':
    databaseConnectionPool = DatabaseConnectionPool("test", 5)
    service = LeagueService( databaseConnectionPool)

    for i in range(25):
        league = service.getLeague(1)
        if (league == None):
            print("No league found for id=1")
        else:
            #print(JsonUtils.json(league))
            print("Single Request #" + str(i))

        leagues = service.list()
        if (leagues == None):
            print("No leagues list found")
        else:
            #print(JsonUtils.jsonList(leagues))
            print("Second request #" + str(i))
