create table league (
    leagueId integer not null auto_increment,
    userId integer not null,
    name varchar(255) not null,
    nameNormalized varchar(255) not null unique,
    dateAdded datetime not null,
    primary key (leagueId)
);

create table user (
    userId integer not null auto_increment,
    name varchar(255) not null,
    nameNormalized varchar(255) not null unique,
    passwordHash varchar(255) not null,
    isGuest bit not null,
    dateAdded datetime not null,
    primary key (userId)
);

alter table league 
    add index league_user_id_idx (userId), 
    add constraint league_user_id_fk
    foreign key (userId) 
    references user (userId);
