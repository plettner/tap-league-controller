from League import League
from LeagueService import LeagueService
from JsonUtils import JsonUtils
from DBCP import DatabaseConnectionPool
from typing import Optional
from fastapi import FastAPI

app = FastAPI()

class LeagueController :

    pool = None
    service = None

    def __init__(self) -> None:
        self.pool = DatabaseConnectionPool("test", 5)
        self.service = LeagueService(self.pool)

    def getLeagues(self):
        leagues = self.service.list()
        # return JsonUtils.jsonList(leagues)
        return leagues

    def getLeague(self, id):
        league = self.service.getLeague(id)
        #return JsonUtils.json(league)
        return league

controller = LeagueController()

@app.get("/leagues")
def getLeagues():
    return controller.getLeagues()

@app.get("/leagues/{id}")
def getLeague(id: int):
    return controller.getLeague(id)
