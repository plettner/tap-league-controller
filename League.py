"""
Encapsulates the data that makes up a Leage
"""
class League:

    def __init__(self, id, userId, name, normalName, dateAdded):
        self.leagueId = id
        self.userId = userId
        self.name = name
        self.nameNormalized = normalName
        self.dateAdded = dateAdded
